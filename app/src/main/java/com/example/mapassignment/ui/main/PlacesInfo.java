package com.example.mapassignment.ui.main;

public class PlacesInfo {

    // string variable for
    // storing streetAddress name.
    private String streetAddress;

    // string variable for storing
    // employee cityName number
    private String cityName;

    // string variable for storing
    // employee address.
    private String countryName;
    private String imageString;

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    // an empty constructor is
    // required when using
    // Firebase Realtime Database.
    public PlacesInfo() {

    }

    // created getter and setter methods
    // for all our variables.
    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}

